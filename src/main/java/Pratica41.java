/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import utfpr.ct.dainf.if62c.pratica.Elipse;
import utfpr.ct.dainf.if62c.pratica.Circulo;

/**
 *
 * @author Ane
 */
public class Pratica41 {
 
    public static void main(String[] args) {
        Elipse e1 = new Elipse(2.5, 3.5);
        Elipse e2 = new Elipse(2, 5);
        Circulo c1 = new Circulo(5);
        Circulo c2 = new Circulo(1.5);

        System.out.println("Perímetro primeira elipse = "+e1.getPerimetro()+" Área primeira elipse = "+e1.getArea());
        System.out.println("Perímetro segunda elipse = "+e2.getPerimetro()+"  Área segunda elipse = "+e2.getArea()); 
        System.out.println("Perímetro primeiro círculo = "+c1.getPerimetro()+" Área primeiro círculo = "+c1.getArea());
        System.out.println("Perímetro segundo círculo = "+c2.getPerimetro()+" Área segundo círculo= "+c2.getArea());
    }        
}
